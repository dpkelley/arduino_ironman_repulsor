
#ifndef _BLUETOOTH_SERIAL_H_
#define _BLUETOOTH_SERIAL_H_
//This is a preprocessor technique of preventing a header file from being included multiple times

#include <SoftwareSerial.h>


class BluetoothSerial : public SoftwareSerial
{
public:

  //default constructor
	BluetoothSerial() : SoftwareSerial(4, 2) {
		vccPin = 3; statusPaired = 13;
		pinMode(vccPin, OUTPUT); digitalWrite(vccPin, HIGH); // declare vccPin as output for bt
		pinMode(statusPaired, INPUT_PULLUP); digitalWrite(statusPaired, LOW);
	};

	BluetoothSerial(int rx, int tx, int vcc, int status) : SoftwareSerial(rx, tx) {
		vccPin = vcc; statusPaired = status;
		pinMode(vccPin, OUTPUT); digitalWrite(vccPin, HIGH); // declare vccPin as output for bt
		pinMode(statusPaired, INPUT_PULLUP); digitalWrite(statusPaired, LOW);   // declare statusPaired as input
		
	}; 
 
  //TODO - autocinfig to baudrate from here, this will call arduino config sketch
  //constructor with baud
	BluetoothSerial(int rx, int tx, int vcc, int status, long baudRate) : SoftwareSerial(rx, tx) {
		
		vccPin = vcc; statusPaired = status;
		pinMode(vccPin, OUTPUT); digitalWrite(vccPin, HIGH); // declare vccPin as output for bt
		pinMode(statusPaired, INPUT_PULLUP); digitalWrite(statusPaired, LOW);   // declare statusPaired as input
		SoftwareSerial::begin(baudRate);
		
	}; 

	/*
	SoftwareSerial inherited methods
		available()
		begin()
		isListening()
		overflow()
		peek()
		read()
		print()
		println()
		listen()
		write() 
	*/
			

public:
  int vccPin;
	int statusPaired;
	String lastTx = "_";


  
	//Overloaded method
    virtual String read(String s) { 
		String data = "";
			if (available()) {
			while(available()) { // While there is more to be read, keep reading.
				data += (char)SoftwareSerial::read();
			}
			data = data.substring(0, data.length() - 1);
			return data;
		}	
	}

	//Overwritten method
	virtual int write(String command){
		for (int i = 0; i < command.length(); i++){
			delay(30);
			SoftwareSerial::write((byte)command.charAt(i));
		}
		SoftwareSerial::write(10); 
			delay(30);
			
	};


    virtual int write(String command, boolean truncate){
    String truncateHderErr = "";
    if(truncate)
      truncateHderErr = "_";

 
  String data = truncateHderErr+command; //this fixes the diamond question mark errors on Androids end
    for (int i = 0; i < data.length(); i++){
      delay(30);
      SoftwareSerial::write((byte)data.charAt(i));
    }
    SoftwareSerial::write(10); 
      delay(30);
      
  };


  
	virtual void sendOnce(String data){
		if (lastTx != data) {
			lastTx = data;
			write(data, true);           
     };  
 }


	
};

#endif // _BLUETOOTH_SERIAL_H_
