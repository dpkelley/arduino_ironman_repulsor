
#ifndef _BLUETOOTH_CONNECTION_H_
#define _BLUETOOTH_CONNECTION_H_
//This is a preprocessor technique of preventing a header file from being included multiple times

#include "BluetoothSerial.h"


  
  

  
class BluetoothConnection : public BluetoothSerial{
  
public:

  //overloaded constructor
  BluetoothConnection(int rx, int tx, int vcc, int status, long baudRate) : BluetoothSerial(rx, tx, vcc, status, baudRate){
  bluetoothConnected = false;
  };


public: //instance vars

boolean bluetoothConnected = false;
String serialData = "_";

boolean isOnline(){  //BluetoothConnection
    if (devicePaired() == false){
      openSocket();
    }
  return (devicePaired() and bluetoothConnected);
  
}

boolean devicePaired(){ //BluetoothConnection
  return digitalRead(statusPaired) == 1;
}


void openSocket(){ //BluetoothConnection
    
        //printLast("bluetooth.statusPaired == 0");
        bluetoothConnected = false;
        //resetIdle(0);
        bluetoothConnected = ifSourceConnected();
        
}

void sendReply(){ //BluetoothConnection
        //printLast("sending ping...");
        write("pingreply", true);
}


boolean ifSourceConnected(){ //BluetoothConnection

  while (bluetoothConnected == false){  
  
    write("handshaking", true);
    //printLast("handshaking");
    //delay(testDelay);  

    serialData = read("OVERLOADED");
    //delay(testDelay); 
    if (serialData == "ping"){
      //printLast(serialData);
      bluetoothConnected = true;
      serialData = "";
      sendReply();

return true;
}
}
 return false;

}



  

  
};
#endif // _BLUETOOTH_CONNECTION_H_
