/*
IRON MAN REPULSOR HAND ONLY
This Arduino sketch has quite a bit of protocol handshaking with an HC-06 bluetooth to pair
with an Android device and the corresponding application made in Android Studio for communication.
The capability allows for commands to be send to the Android device so that features like audio 
files can be offloaded from the Arduino to scale down the extra electronics involved in making 
Arduino play sound. Also, Arduino can only handle low frequency samples for sound as a simple circuit
can only go uo to 11hz where as a device like Raspberry Pi can handle audio at 44.hz. Although Raspberry Pi
was initioally developed with this protocol, it was later scratched as Raspberry Pi was not fast enough at
multitasking handlers for multiple features. Android devices already have all the necessary services and are
more than capable of handling mre than one bluetooth connection for a seamless system. In addition, because
Andoid already has services like Google voice, the features of voic recognition and possibly a VR or 
AR(Augmented Reality) component integration could later be possible to simulate other features.
*/
//1200,2400,4800,9600,19200,38400,57600,115200,230400,460800,921600,1382400
#include "BluetoothConnection.h"


BluetoothConnection bluetooth(12, 11, 13, 10, 115200); // RX, TX, VCC, STATUS, BAUD


//STAUS = pair mode (blinking = NOT paired = 0v, solid LED = paired = 3.3v)
//RX is the label on the bluetooth to the pin specified on the Arduino
//TX is the label on the bluetooth to the pin specified on the Arduino
//VCC controls the +3.3v supply to the bluetooth programatically for the the purpose uploading sketches without disconnecting
//BAUD sets the rate for the BT and to not be confused with Serial COM port baud rate


  //PINS    7,8,9,10,11   
  int blastArrayLEDs = 2; // choose the pin for the L 
  int chargeArrayLEDs = 3; // choose the pin for the L  
  int blueCenterLED = 4; // choose the pin for the L  8
  
  int wristBtn = 7; // choose the input pin (for a pushbutton)
  int magneticBtn = 6;   // choose the input pin (for a pushbutton) //Changed from 6
  int airPump = 14; // choose the input pin (for a pushbutton)
  int vapeCoil = 5; // choose the input pin (for a pushbutton)
  
  
  //VARS
  int fadeIncrement = 7;
  int brightness = 0;    // how bright the LED is
  int fadeAmount = 5;    // how many points to fade the LED by
  int idleValue = 12;
  int fadeFinal = idleValue;
  int blast = 0;
  int testDelay = 33;  //no less than 30
  int broken = HIGH;
  int grounded = LOW;

  String turn = "0";
  String printLastCommand = "_";


  
// runs on startup ------
void setup()
{
  Serial.begin(9600);//115200
  pinMode(chargeArrayLEDs, OUTPUT);  // declare LED as output
  pinMode(blueCenterLED, OUTPUT);  // declare LED as output
  
  pinMode(wristBtn, INPUT_PULLUP); digitalWrite(wristBtn, HIGH);  // declare wristBtn as input, no external resistor needed
  pinMode(magneticBtn, INPUT_PULLUP); digitalWrite(magneticBtn, HIGH);  // declare magneticBtn as input, no external resistor needed

  pinMode(airPump, INPUT_PULLUP); analogWrite(airPump, 170);  //170 = 3V
  pinMode(vapeCoil, INPUT_PULLUP); analogWrite(vapeCoil, 0); //209 = 3.7V

}




    
void loop()
{  

      bluetooth.serialData = bluetooth.read("OVERLOADED"); //
  
      if (chargeEngaged()) { 
        repulsorCharge(wristBtn, 128);  //72
      }
      
      if (dischargeEngaged()){ 
        repulsorDischarge();
      }
      
      if (menuEngaged()){ 
        bluetooth.sendOnce("setup");
      }
      
      if (nothingEngaged()){ 
        //bluetooth.sendOnce("nothing");
      }
      
      if (flightEngaged()) { 
        fly();
      }

      if (bluetooth.isOnline()){
      }

               
}



boolean chargeEngaged(){
  return bluetooth.isOnline() and digitalRead(wristBtn) == grounded and digitalRead(magneticBtn) == grounded; //grounded = LOW
}


boolean dischargeEngaged(){
  return bluetooth.isOnline() and digitalRead(wristBtn) == broken and digitalRead(magneticBtn) == grounded; //broken = HIGH
}


boolean menuEngaged(){
  return bluetooth.isOnline() and digitalRead(wristBtn) == broken and digitalRead(magneticBtn) == grounded and bluetooth.lastTx != "dis"; //broken = HIGH
}


boolean nothingEngaged(){
  return bluetooth.isOnline() and digitalRead(wristBtn) == broken and digitalRead(magneticBtn) == broken; //broken = HIGH
}


boolean flightEngaged(){
  return bluetooth.isOnline() and digitalRead(wristBtn) == grounded and digitalRead(magneticBtn) == broken; //grounded = LOW
}


boolean released(int button){ return digitalRead(button) == broken;}

////////////////////CLASS METHODS////////////////////


 void printLast(String data){ 
  
      if (printLastCommand != data) {
        printLastCommand = data;
          Serial.println(data);               
      }  
 }




void chargeLEDs(int maxLumens){
  
  
if (fadeFinal < maxLumens){
         analogWrite(chargeArrayLEDs, fadeFinal);
              if (fadeFinal > maxLumens -(maxLumens/fadeIncrement)){
                  analogWrite(blueCenterLED, fadeFinal);}
         delay(testDelay);
         fadeFinal += fadeIncrement;
          }else if (fadeFinal >= maxLumens){
            fadeFinal = maxLumens;
          analogWrite(chargeArrayLEDs, fadeFinal);
          analogWrite(blueCenterLED, fadeFinal);
            
         delay(testDelay);
         
          }
}




void repulsorDischarge(){
  String data = "dis";
    if (turn == "1"){
      bluetooth.sendOnce(data);
      turn = "0";
    }
    
            for (int fadeValue = fadeFinal ; fadeValue > idleValue; fadeValue -= fadeIncrement) {
            analogWrite(chargeArrayLEDs, fadeValue);
            analogWrite(blueCenterLED, 0);
            delay(testDelay);
            fadeFinal = fadeValue;
            }
    
    blast = 0;
          
}

 
void repulsorCharge(int button, int maxLumens){
  String data = "charge";
  printLast(data);
      if (turn == "0"){
        if( released(button)) return;
        bluetooth.sendOnce(data);
        turn = "1";
        } 
        if( released(button)) return;
       
          chargeLEDs(maxLumens); 
          
            if (digitalRead(magneticBtn) == broken && (digitalRead(wristBtn) == grounded) && blast == 0 )
            { blastLEDs(button);}
            if (digitalRead(magneticBtn) == grounded && (digitalRead(wristBtn) == grounded) && blast == 1 )
            { blast = 0;}
            if ((digitalRead(wristBtn) == broken) )
            { return;}
}


void blastLEDs(int button){
  String data = "blast";
              if (blast == 0){ 
                bluetooth.sendOnce(data);blast = 1;turn = "0";
                delay(333);
                analogWrite(chargeArrayLEDs, 255);
                analogWrite(blueCenterLED, 255);
                analogWrite(blastArrayLEDs, 255);
                delay(200);}
        delay(200);
        resetIdle(fadeFinal);
}

void resetIdle(int amount){
      analogWrite(chargeArrayLEDs, amount);
      analogWrite(blueCenterLED, 0);
      analogWrite(blastArrayLEDs, 0);
}

void fly(){
analogWrite(airPump, 170); //170 = 3V
  
}
